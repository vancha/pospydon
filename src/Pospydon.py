#!/usr/bin/python3
import gi

gi.require_version('Notify', '0.7')
gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Pango', '1.0')

import json
import six
from mastodon import Mastodon, streaming
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout
from requests.exceptions import ChunkedEncodingError, ReadTimeout
from gi.repository import Notify, Gio, Gtk
from gi.repository import Pango
from gi.repository.GdkPixbuf import Pixbuf
from LoginView import LoginView
from HomeTimelineView import HomeTimelineView
from TootView import TootView
#from bs4 import BeautifulSoup

TOOTVIEW = 0;
CLIENTCREDENTIALS_LOCATION = "pospydon_clientcred.secret";
USERCREDENTIALS_LOCATION = "pospydon_usercred.secret";














class Views(Gtk.Box):


    def loginHandler(self,obj,args):
        print('got object:',type(args))
        self.Mastodon = args
        tv = TootView(self.Mastodon)
        home = HomeTimelineView(mastodonx=self.Mastodon)
        #self.views = [tv,home]
        #self.ss.set_stack(self.stack)
        self.stack.add_titled(tv,"Toot","Toot")
        self.stack.add_titled(home,"Home","Home")
        #self.widgetToDestroy = self.stack.get_visible_child()
        self.stack.set_visible_child_name('Home')
        self.stackswitcher.set_stack(self.stack)
        self.pack_start(self.stackswitcher,True, True, 0)
        self.pack_start(self.stack, True, True, 1)

    def __init__(self,Mastodon):
        self.Mastodon = Mastodon
        Gtk.Box.__init__(self,orientation=Gtk.Orientation.VERTICAL, spacing=6);
        self.stack = Gtk.Stack()
        self.stackswitcher = Gtk.StackSwitcher()
        self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.stack.set_transition_duration(1)
        self.lv = LoginView(self.Mastodon)
        self.lv.connect('testSignal',self.loginHandler)
        self.stack.add_titled(self.lv,'login','login')
        self.stack.set_visible_child_name('login')
        self.stackswitcher.set_stack(self.stack)
        self.stackswitcher.props.halign = Gtk.Align.CENTER;
        self.pack_start(self.stackswitcher,True, True, 1)

        self.pack_start(self.stack, True, True, 1)



        '''
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
        self.label = Gtk.Label('What instance do you want to connect to?')
        box.pack_start(self.label,False,False,0)
        self.instanceentry = Gtk.Entry();
        self.instanceentry.set_placeholder_text("https://instancename.org")
        button = Gtk.Button("Next");
        button.connect('clicked',self.login_button_pressed)
        box.pack_start(self.instanceentry,False,False, 0)
        box.pack_start(button,False,False, 0)

        box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
        self.emaillabel = Gtk.Label('What is your account email adress?')
        self.emailentry = Gtk.Entry();
        self.passwordlabel = Gtk.Label('What is your account password?')
        self.passwordentry = Gtk.Entry();
        button2 = Gtk.Button("Login");
        button2.connect('clicked',self.login_button_pressed)
        box2.pack_start(self.emaillabel,False,False,0)
        box2.pack_start(self.emailentry,False,False,0)
        box2.pack_start(self.passwordlabel,False,False,0)
        box2.pack_start(self.passwordentry,False,False,0)
        box2.pack_start(button2,False,False, 0)


        self.add_titled(box,"instance","instance")
        self.add_titled(box2,"user","user")'''

'''class tootView(Gtk.Box): #make every view a class,with each their own model
    def tootbuttonclicked(self,widget,data):
        #global mastodon;
        print(self.mastodon.toot(self.entry.get_text()))

    def favoriteToot(self,id):
        print('favoritating toot with id ' + id);
        self.mastodon.status_favourite(id)

    def sanitizeStatus():
        print('sanitizing the status thing')

    def getToots(self,model):

        global mastodon;

        timeline = self.mastodon.timeline(timeline='home',limit=None)
        for toot in timeline:
            contentstring = toot['content'];
            soup = contentstring#BeautifulSoup(contentstring,'html.parser')
            x = str('+1');
            print('id\'s:'+ str(toot['id']))
            self.model.append([str(toot['id']),"  "+toot['account']['username']+ "  ",soup.get_text(),x])
        #print("getting those toots!")


    def __init__(self, mastodonx):
        print('tootinditoots!')
        self.mastodon = mastodonx
        Gtk.Box.__init__(self,orientation=Gtk.Orientation.VERTICAL,spacing=5);

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
        self.entry = Gtk.Entry();
        button = Gtk.Button("Toot");
        button.connect('clicked',self.tootbuttonclicked, self.mastodon)
        box.pack_start(self.entry,True,True, 0)
        box.pack_start(button,True,True, 0)

        self.pack_start(box,True,True,0);

        #streamlistener = PospydonStreamListener()
        #connects the actual streamlistener
        #streamhandle = mastodon.stream_user(streamlistener,run_async=True)'''


class Pospydon(Gtk.ApplicationWindow):

    def __init__(self):
        Gtk.ApplicationWindow.__init__(self, title="Pospydon")
        print("initing");
        self.set_border_width(10);
        self.set_default_size(200,400)
        #self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
        #self.add(self.box)
        self.mastodon = Mastodon;
        self.box = Views(self.mastodon)
        self.add(self.box)
        self.box.show_all()

        '''clientsecrets = Gio.file_new_for_path(CLIENTCREDENTIALS_LOCATION);
        usersecrets = Gio.file_new_for_path(USERCREDENTIALS_LOCATION);
        if(not (clientsecrets.query_exists() == True and usersecrets.query_exists() == True)):
            print('secrets does not exist, logged in')

            self.box.pack_start(self.stack,True,True,0)
        else:
            self.login();

            self.stack=Gtk.Stack();
            self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
            self.stack.set_transition_duration(500)
            self.tootview = tootView(self.mastodon);
            self.home = home(mastodonx=self.mastodon)
            self.notifications = notifications(mastodonx=self.mastodon);
            self.stack.add_titled(self.tootview,'TOOT','toot');
            self.stack.add_titled(self.home,'HOME','home');
            self.stack.add_titled(self.notifications,'NOTIFICATIONS','notification')
            self.switcher = Gtk.StackSwitcher()
            self.switcher.set_hexpand(True)
            self.switcher.set_stack(self.stack)
            self.box.pack_start(self.switcher,False,False,0)
            self.box.pack_start(self.stack,True,True,0);'''



if __name__ == '__main__':
  window = Pospydon();
  window.connect("destroy",Gtk.main_quit)
  window.show_all();
  Gtk.main();
