'''
Change from stack to treeview, to implement in Pospydon.py "view"

'''

import gi

gi.require_version('Gtk', '3.0')

import json
import six
#from PospydonStreamListener import PospydonStreamListener
from mastodon import Mastodon, streaming
#from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout
#from requests.exceptions import ChunkedEncodingError, ReadTimeout
from gi.repository import Pango
from gi.repository.GdkPixbuf import Pixbuf
from gi.repository import Notify, Gio, Gtk
#from bs4 import BeautifulSoup

class TootView(Gtk.Box): #make every view a class,with each their own model
    def tootbuttonclicked(self,widget,data):
        #global mastodon;
        print(self.mastodon.toot(self.entry.get_text()))

    def favoriteToot(self,id):
        print('favoritating toot with id ' + id);
        self.mastodon.status_favourite(id)

    def sanitizeStatus():
        print('sanitizing the status thing')

    def getToots(self,model):
        '''get more toots by specifying  three parameters:
        # since_id,
 	max_id
        limit.

	since_id allows you to specify the smallest id you want in the returned data.
	max_id, similarly, allows you to specify the largest.
	By specifying either one (generally, only one, not both) of them you can go through pages forwards and backwards.'''

        #global mastodon;

        timeline = self.mastodon.timeline(timeline='home',limit=None)
        for toot in timeline:
            contentstring = toot['content'];
            soup = contentstring#BeautifulSoup(contentstring,'html.parser')
            x = str('+1');
            print('id\'s:'+ str(toot['id']))
            self.model.append([str(toot['id']),"  "+toot['account']['username']+ "  ",soup.get_text(),x])
        #print("getting those toots!")


    def __init__(self, mastodonx):
        self.mastodon = mastodonx
        Gtk.Box.__init__(self,orientation=Gtk.Orientation.VERTICAL,spacing=5);

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
        self.entry = Gtk.Entry();
        button = Gtk.Button("Toot");
        button.connect('clicked',self.tootbuttonclicked, self.mastodon)
        box.pack_start(self.entry,True,True, 0)
        box.pack_start(button,True,True, 0)

        self.pack_start(box,True,True,0);
        self.show_all()

        #streamlistener = PospydonStreamListener()
        #connects the actual streamlistener
        #streamhandle = mastodon.stream_user(streamlistener,run_async=True)
