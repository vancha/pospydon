import gi

gi.require_version('Pango', '1.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Pango
from bs4 import BeautifulSoup

class HomeTimelineView(Gtk.Box): #make every view a class,with each their own model
    def tootbuttonclicked(self,widget,data):
        print(self.mastodon.toot(self.entry.get_text()))

    def favoriteToot(self,id):
        print('favoritating toot with id ' + id);
        self.mastodon.status_favourite(id)

    def sanitizeStatus():
        print('sanitizing the status thing')

    def getToots(self,model):
        global mastodon;

        timeline = self.mastodon.timeline(timeline='home',limit=None)
        for toot in timeline:
            contentstring = toot['content'];
            soup = BeautifulSoup(contentstring,'html.parser')
            x = str('+1');
            print('id\'s:'+ str(toot['id']))
            self.model.append([str(toot['id']),"  "+toot['account']['username']+ "  ",soup.get_text(),x])

    def getNotifications(self,model):
        print('getting those notifications')

    def rowselected(self,treeview,path,view_column):
        model = treeview.get_model()
        if(view_column.get_title() == 'Like'):
            self.favoriteToot((model[path][0]))

    def __init__(self, mastodonx):
        self.mastodon = mastodonx
        Gtk.Box.__init__(self,orientation=Gtk.Orientation.VERTICAL,spacing=5);

        home = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
        scrolledview = Gtk.ScrolledWindow()
        scrolledview.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.model = Gtk.ListStore.new([str,str,str,str])
        self.getToots(self.model);

        treeview = Gtk.TreeView(self.model);
        treeview.set_property('activate-on-single-click',True);
        treeview.connect('row-activated',self.rowselected);

        cellrenderer = Gtk.CellRendererText();
        cellrenderer.props.wrap_width = 500;
        cellrenderer.props.wrap_mode = Pango.WrapMode.WORD

        column = Gtk.TreeViewColumn("user",cellrenderer,text=1)
        column2 = Gtk.TreeViewColumn("status",cellrenderer,text=2)
        column3 = Gtk.TreeViewColumn("Like",cellrenderer, text=3)
        treeview.append_column(column)
        treeview.append_column(column2)
        treeview.append_column(column3)


        scrolledview.add(treeview)
        self.pack_start(scrolledview, True, True,1);
        self.show_all()

