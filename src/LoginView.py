import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Notify, Gio, Gtk,GObject
from mastodon import Mastodon, streaming
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout

CLIENTCREDENTIALS_LOCATION = "pospydon_clientcred.secret";
USERCREDENTIALS_LOCATION = "pospydon_usercred.secret";

class LoginView(Gtk.Box):
    '''def login_button_pressed(self, widget):
    if(widget.get_label() == "Login"):
       if(self.instanceentry.get_text() != "" and self.emailentry.get_text() != "" and self.passwordentry.get_text() != ""):#prevent registering more than once
           client_cred = Gio.file_new_for_path(CLIENTCREDENTIALS_LOCATION)
           user_cred = Gio.file_new_for_path(USERCREDENTIALS_LOCATION)
           if(not client_cred.query_exists() == True or not user_cred.query_exists()):
               try:
                   self.Mastodon.create_app(
                       'pospydon',
                       api_base_url = self.instanceentry.get_text(),
                       to_file = CLIENTCREDENTIALS_LOCATION
                   )
                   self.mastodon = Mastodon(
                       client_id = CLIENTCREDENTIALS_LOCATION,
                       api_base_url = self.instanceentry.get_text()
                   )
                   self.mastodon.log_in(
                       self.emailentry.get_text(),
                       self.passwordentry.get_text(),
                       to_file = USERCREDENTIALS_LOCATION
                   )
               except Exception as e:
                   print('cant register app '+ str(e))
           print('all fields filled in, well done')
    else:
        active = self.get_child_by_name('user')
        self.set_visible_child(active)'''


    @GObject.Signal(flags=GObject.SignalFlags.RUN_LAST, return_type=str,
                    arg_types=(object,),
                    accumulator=GObject.signal_accumulator_true_handled)
    def testSignal(self, *args):
        print("Handler", args)


    def login(self, widget):
        client_cred = Gio.file_new_for_path(CLIENTCREDENTIALS_LOCATION)
        user_cred = Gio.file_new_for_path(USERCREDENTIALS_LOCATION)
        if(not client_cred.query_exists()): #usercredentials does not exist, assume user has not logged in so clientcredentials also don´t exist
             print('client not yet registered')
             self.mastodon.create_app(
                 'Pospydon',
                 website = 'https://github.com/vancha/pospydon',
                 api_base_url = self.instanceEntry.get_text(),
                 to_file = CLIENTCREDENTIALS_LOCATION
            )
             self.mastodon = Mastodon(
                 client_id = CLIENTCREDENTIALS_LOCATION,
                 api_base_url = self.instanceEntry.get_text(),
            )
             self.mastodon.log_in(
                self.emailEntry.get_text(),
                self.passwordEntry.get_text(),
                to_file = USERCREDENTIALS_LOCATION
            )
             self.emit("testSignal",self.mastodon)
        else:
            print('Client already registered.')
            self.mastodon = Mastodon(
              access_token = USERCREDENTIALS_LOCATION,
              api_base_url = self.instanceEntry.get_text()
            )
            self.emit("testSignal",self.mastodon)
            '''self.mastodon = Mastodon(
                access_token = 'pospydon_usercred.secret',
                api_base_url = 'https://mastodon.sdf.org'
            )
            self.emit("testSignal",self.mastodon)'''

        '''self.mastodon = Mastodon(
            client_id = 'pospydon_clientcred.secret',
            api_base_url = 'https://mastodon.sdf.org'
        )
        self.mastodon.log_in(
            'tjipkevdh@gmail.com',
            'Doradehond!!2',
            to_file = 'pospydon_usercred.secret'
        )
        self.mastodon = Mastodon(
            access_token = 'pospydon_usercred.secret',
            api_base_url = 'https://mastodon.sdf.org'
        )
        self.emit("testSignal",self.mastodon)'''

    def __init__(self,mastodon):
        self.mastodon = mastodon
        print('opening login window')
        Gtk.Box.__init__(self,orientation=Gtk.Orientation.VERTICAL,spacing=5);
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
        self.instanceLabel = Gtk.Label("What's your instance name?");
        box.pack_start(self.instanceLabel, True, True, 0);
        self.instanceEntry = Gtk.Entry();
        box.pack_start(self.instanceEntry, True, True, 0);
        self.emailLabel = Gtk.Label("What's your email address?");
        box.pack_start(self.emailLabel, True, True, 0);
        self.emailEntry = Gtk.Entry();
        box.pack_start(self.emailEntry, True, True, 0);
        self.passwordLabel = Gtk.Label("What's your password?");
        box.pack_start(self.passwordLabel, True, True, 0);
        self.passwordEntry = Gtk.Entry();
        box.pack_start(self.passwordEntry, True, True, 0);
        self.LoginButton = Gtk.Button("Attempt login");
        box.pack_start(self.LoginButton, True, True, 0);
        self.pack_start(box,True,True,0);
        self.LoginButton.connect('clicked', self.login)
        #self.emit("testSignal")
        #streamlistener = PospydonStreamListener()
        #connects the actual streamlistener
        #streamhandle = mastodon.stream_user(streamlistener,run_async=True)

